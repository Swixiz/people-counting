import ultralytics
from ultralytics import YOLO

import roboflow

"""
Downloads the dataset using the Roboflow api. Replace the KEY parameter by your key to log in 
your Roboflow account. For security reasons, it has been removed from the source code. 
"""
def download_dataset():
    rf = roboflow.Roboflow(api_key="KEY")
    project = rf.workspace("tp3-perception-ai-iwfjw").project("people-counting-in-depth-images")
    project.version(3).download("yolov8")

"""
Transfer learning on the YoloV8 nano model. Fine tuning over the training sequence.
"""
def train_yolo():
    model = YOLO("yolov8n.pt")
    model.train(data="People-counting-in-Depth-images-3/data.yaml", epochs=200, imgsz=640)


if __name__ == "__main__":
    download_dataset()
    train_yolo()
