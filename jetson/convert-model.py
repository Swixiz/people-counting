import ultralytics 
from ultralytics import YOLO 

# Retrieves the model trained in the Colab environment to export it as a TensorRt model,
# which is more optimized on the jetson board.
model = YOLO("best.pt")
model.export(format='engine', device=0)
