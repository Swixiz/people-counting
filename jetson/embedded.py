import ultralytics
import cv2 as cv
from ultralytics import YOLO
import glob
import roboflow

"""
Downloads the dataset using the Roboflow. It replaces the values of the malformed data.yaml
and rewrites it into the same file.

Replace the KEY parameter by your own key to connect to your roboflow account. For security reasons,
our key-api has been removed. 
"""
def download_dataset():
    rf = roboflow.Roboflow(api_key="KEY")
    project = rf.workspace("tp3-perception-ai-iwfjw").project("people-counting-in-depth-images")
    project.version(3).download("yolov8")
    with open('People-counting-in-Depth-images-3/data.yaml', 'r') as file:
        data = file.read()
    data=data.replace('People-counting-in-Depth-images-3', '')
    data=data.replace('path: ../datasets/roboflow', 'path: People-counting-in-Depth-images-3')
    with open('People-counting-in-Depth-images-3/data.yaml', 'w') as file:
        file.write(data)

"""
Uses the pre-trained optimized model to detect the people in pictures from a given sequence. 
It limits the number of samples to the given parameter.  
"""
def soft_detect(model, sequence, samples):
    index = 0
    for filename in sorted(glob.glob(f"People-counting-in-Depth-images-3/test/images/{sequence}*.jpg")):
        print(filename)
        if index >= samples:
            break
        img = cv.imread(filename)
        res = model.predict(img, device=0)
        img_box = res[0].plot()
        cv.imshow('Image', img_box)
        cv.waitKey(0)
        index+=1
        cv.destroyAllWindows()

if __name__ == "__main__":
    download_dataset()
    yolo = YOLO("best.engine", task="detect")
    soft_detect(yolo, "PASS", 20)
