import cv2 as cv
import ultralytics
from ultralytics import YOLO
import glob
import numpy as np

"""
This function is used to detect the people in a sequence of pictures. It takes the trained YoloV8 model,
the folder containing the pictures and the name of the sequence to target. It displays both the boxes, the center of the
boxes and the distances between each person. It creates a video from all the pictures.  
"""
def detect(model, folder, sequence):
    img_array = []
    # Sorts all the pictures in increasing order to recreate the 
    for file in sorted(glob.glob(f'{folder}/{sequence}*.jpg')):
        img = cv.imread(file)
        res = model.predict(img)
        img_box = res[0].plot()

        # Detect and plot centroid
        centers = []
        for box in res[0].boxes:
            x = int(box.xywh[0,0].item())
            y = int(box.xywh[0,1].item())
            centers.append((x,y))
            cv.circle(img_box, (x, y), 5, (0, 0, 255), -1)

        # Prints the number of people
        cv.putText(img_box, f'People count: {len(centers)}', (20, 20), cv.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 0), 1)
        # Plot distances
        if len(centers) >= 2:
            for index1 in range(len(centers)):
                for index2 in range(index1+1, len(centers)):
                    c0 = centers[index1]
                    c1 = centers[index2]
                    cv.line(img_box, c0, c1, (0, 255, 0), 2)
                    dist = round(np.linalg.norm(np.array(c1) - np.array(c0)), 0)
                    cv.putText(img_box, f'Distance between : {index1} and {index2}: ' + str(dist), (20, 20*(index1+index2+1)), cv.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 0), 1)

        img_array.append(img_box)
    # Creates the video sequence from all the images
    out = cv.VideoWriter('test2.avi',cv.VideoWriter_fourcc(*'DIVX'), 15, (640,640))
    for i in range(len(img_array)):
        out.write(img_array[i])
    out.release()

if __name__ == "__main__":
    model = YOLO("best.pt")
    detect(model, "People-counting-in-Depth-images-3/test/images", "PASS")