import cv2 as cv
import glob
import os
import numpy as np

"""
This function is used to transform the data acquired using the Depth camera.
The pictures must be normalized to be able to visualize it and to label it in Roboflow.
"""
def normalize(folder):
    for filename in glob.glob(f'{folder}/*.png'):
        frame = cv.imread(filename)
        # Normalize the image
        img_normalized = cv.normalize( frame, None, 0, 1.0, cv.NORM_MINMAX, dtype=cv.CV_32F )
        cv.imshow("orig",frame)
        cv.imshow( "norm", img_normalized)
        filename = os.path.join("dataset/export_folder", os.path.basename(filename))
        print(f"Saving file {filename}")
        cv.imwrite(filename, (img_normalized*255).astype(np.uint8))
        # close all windows
        cv.destroyAllWindows()

if __name__ == "__main__":
    normalize("dataset/Depth_CROSS_X-F1-B1_P880043_20200625111459_225")