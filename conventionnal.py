import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import imutils

def color_detection(image):
    image = cv.imread(image) # load the image
    rgb = cv.cvtColor(image, cv.COLOR_BGR2RGB) # convert image to RGB color space
    plt.title("RGB")
    plt.imshow(rgb)
    plt.show()

    lower = np.array([0, 10, 0], dtype="uint8") # lower bound for each channel
    upper = np.array([130, 255, 255], dtype="uint8") # upper bound for each channel
    mask = cv.inRange(rgb, lower, upper) # create a mask from the bounds
    output = cv.bitwise_and(rgb, rgb, mask=mask) # apply the mask to the image
    plt.title("Mask")
    plt.imshow(output)
    plt.show()

    return output

def contours_detection(image):
    # load the image and blur it slightly to remove noise
    gray = cv.GaussianBlur(image, (7, 7), 0)

    # perform edge detection, then perform a dilation + erosion to close gaps in between object edges
    edged = cv.Canny(gray, 50, 100)
    edged = cv.dilate(edged, None, iterations=1) # dilate the image to fill in holes, then find contours on image
    edged = cv.erode(edged, None, iterations=1) # erode the image to reduce the size of the foreground object
    cv.imshow("edged", edged)

    # find contours in the edge map
    cnts = cv.findContours(edged.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    # loop over the contours individually
    for c in cnts:
        # if the contour is not sufficiently large, ignore it
        if cv.contourArea(c) < 500:
            continue

        # compute the Convex Hull of the contour
        hull = cv.convexHull(c)

        # draw the contour and Convex Hull on the image
        cv.drawContours(image, [hull], -1, (0, 255, 0), 2)
        # add a label next to the shape
        M = cv.moments(hull) # compute the center of mass of the contour
        cX = int(M["m10"] / M["m00"]) # compute the center of mass of the object
        cY = int(M["m01"] / M["m00"]) # compute the center of mass of the object

        # write person label
        cv.putText(image, "person", (cX - 10, cY - 10), cv.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255), 2)

    # show the output image
    cv.imshow("output", image)
    cv.waitKey(0)

if __name__ == "__main__":
    output = color_detection("image.jpg")
    contours_detection(output)
    pass